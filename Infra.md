# Infraestrutura do Projeto de Microsserviço 📦

Allan, Leonardo, Kevin, Arlindo

Tarefa 5 de PM

## Implementação 📝

- Spring Boot

## Repositório 📚

Dois repositórios remotos espelhados*

- Gitlab: uso do CI/CD e integração com SonarCloud

    [tag para entrega](https://gitlab.com/jrmsrs1/spring-boot-helloworld/tree/tarefa5)

- Github: necessário para a plataforma de deployment Java

    [tag para entrega](https://github.com/jrmsrs/spring-boot-helloworld/tree/tarefa5)

*talvez consiga usar só o Github

## Deploy 🚀

- ~~Vercel: plataforma gratuita, mais completa~~ *é disponível apenas para Javascript*
- Railway: plataforma com plano gratuito, mas com algumas limitações

    [link](https://spring-boot-helloworld.up.railway.app/)

## SonarCloud 📊

[link](https://sonarcloud.io/dashboard?id=jrmsrs1_spring-boot-helloworld)

## Biblioteca de Testes 🧪

- O padrão do Spring Boot (JUnit + Mockito)

## Documentação auto-gerada 📖

- Swagger-UI

    [link](https://spring-boot-helloworld.up.railway.app/swagger-ui.html)

## Ambiente de Desenvolvimento 🛠️

VS Code, com os plugins:

- Extension Pack for Java: essencial 
- Spring Boot Dashboard: as the name says, dashboard que mapeia os endpoints, beans, etc
- SonarLint: aponta problemas que o Sonar apontaria, durante o desenvolvimento
- Git Graph: interface Git fácil de usar 
